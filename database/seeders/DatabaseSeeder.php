<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $allCategories = Category::all();
        foreach ($allCategories as $category)
        {
            Article::factory()->count(1)->create(['category_id' => $category->id, 'user_id' => 1]);
            Article::factory()->count(1)->create(['category_id' => $category->id, 'user_id' => 2]);
        }
        $allArticles = Article::all();
        foreach ($allArticles as $article)
        {
            Comment::factory()->count(1)->create(['user_id' => 3, 'article_id' => $article->id, 'is_approved_status' => 1]);
            Comment::factory()->count(1)->create(['user_id' => 4, 'article_id' => $article->id]);
        }
    }
}
