@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Создание статьи</h1>
        <div style="text-align: center">
        <form method="post" action="{{route('articles.store')}}">
            @csrf
            <div>
                <p>Заголовок</p>
                <input type="text" name="title" id="title" value="{{old('title')}}" required>
                @error('title')
                    <div class="alert alert-danger">Заголовок обязателен и должен быть не менее 5 и не более 30 символов!</div>
                @enderror
            </div>
            <div>
                <p>Содержание</p>
                <textarea name="body" id="body" required>{{old('body')}}</textarea>
                @error('body')
                <div class="alert alert-danger">Содержание обязательно и должно быть не менее 100 и не более 500 символов!</div>
                @enderror
            </div>
            <div>
                <p>Выберите категорию</p>
                <select name="category_id" id="category_id">
                    @foreach($categories as $category)
                        <option name="category_id" value="{{$category->id}}">{{$category->title}}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" name="user_id" value="{{$user}}">
            <button type="submit">Создать</button>
        </form>
        </div>
    </div>
@endsection
