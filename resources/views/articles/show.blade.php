@extends('layouts.app')
@section('content')
    <div class="container">
        <div>
            <h1 style="text-align: center">{{$article->title}}</h1>
            @if(isset($user) && $user->id == $article->user->id)
                <a href="{{route('articles.edit', compact('article'))}}" style="margin-right: 30px; color: black; text-decoration: none">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                        <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                    </svg>
                </a>
                <form method="post" action="{{route('articles.destroy', compact('article'))}}" style="float: right; margin-right: 1210px">
                    @csrf
                    @method('DELETE')
                    <button style="border: none; background-color: transparent">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-minus" viewBox="0 0 16 16">
                            <path d="M5.5 9a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                            <path d="M14 4.5V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h5.5L14 4.5zm-3 0A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h-2z"/>
                        </svg>
                    </button>
                </form>
            @endif
            <div>
                <p>
                    {{$article->body}}
                </p>
            </div>
            <div style="text-align: right">
                <a href="{{route('profile', compact('user'))}}">Автор: {{$article->user->name}}</a>
            </div>
            <div style="text-align: right">
                <h1>Дата создания: {{$article->created_at}}</h1>
            </div>
            <div style="text-align: right">
                <h1>Дата публикации: {{$timePub}}</h1>
            </div>
        </div>
        <div>
            <h2>Комментарии</h2>
            @if(\Illuminate\Support\Facades\Auth::check() == true && \Illuminate\Support\Facades\Auth::id() != $article->user->id)
                <div style="border: 1px solid black; padding: 40px">
                Оцените статью по нескольким критериям:
            <form method="post" action="{{route('rate', compact('article'))}}">
                @csrf
                @method('PATCH')
                <div style="float: left; margin-right: 40px">
                    <p>Качество статьи</p>
                    <select name="quality">
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                    </select>
                    @error('quality')
                    <div class="alert alert-danger">Поле для заполнения комментария обязательно и должно содержать от 10 до 200 символов</div>
                    @enderror
                </div>
                <div>
                    <p>
                        Актуальность статьи
                    </p>
                    <select name="actuality">
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                    </select>
                    @error('actuality')
                    <div class="alert alert-danger">Поле для заполнения комментария обязательно и должно содержать от 10 до 200 символов</div>
                    @enderror
                </div>
                <div>
                    <p>
                        Понравилась ли статья
                    </p>
                    <div>
                        <input type="radio" id="yes" name="like" value="1" />
                        <label for="yes">Да</label>

                        <input type="radio" id="no" name="like" value="0" />
                        <label for="no">Нет</label>
                    </div>
                    @error('like')
                    <div class="alert alert-danger">имволов</div>
                    @enderror
                </div>
                <input type="hidden" value="{{$article->title}}" name="title">
                <input type="hidden" value="{{$article->body}}" name="body">
                <input type="hidden" value="{{$article->user->id}}" name="user_id">
                <input type="hidden" value="{{$article->category->id}}" name="category_id">
                @error('category_id')
                <div class="alert alert-danger">Поле для заполнения комментария обязательно и должно содержать от 10 до 200 символов</div>
                @enderror
                <button type="submit">Отпрвить</button>
            </form>
                </div>
            @endif
            @if(\Illuminate\Support\Facades\Auth::check() == true)
                Оставьте комментарий!
                <form method="post" action="{{route('comments.store')}}">
                    @csrf
                    <textarea name="body" id="body" placeholder="Напишите ваше мнение...">{{old('body')}}</textarea><br>
                    @error('body')
                    <div class="alert alert-danger">Поле для заполнения комментария обязательно и должно содержать от 10 до 200 символов</div>
                    @enderror
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <input type="hidden" name="article_id" value="{{$article->id}}">
                    <button>Отправить</button>
                </form>
            @endif
            <div>
                @foreach($article->comments as $comment)
                    @if($comment->is_approved_status == 1)
                        <div style="border: 1px solid black; margin-bottom: 20px; padding: 5px">
                            <h2>{{$comment->user->name}}</h2>
                            @if(isset($user) && $user->id == $comment->user->id)
                                <a href="{{route('comments.edit', compact('comment'))}}" style="margin-right: 30px; color: black; text-decoration: none">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                        <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                    </svg>
                                </a>
                                <form method="post" action="{{route('comments.destroy', compact('comment'))}}" style="float: right; margin-right: 1210px">
                                    @csrf
                                    @method('DELETE')
                                    <button style="border: none; background-color: transparent">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-minus" viewBox="0 0 16 16">
                                            <path d="M5.5 9a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                                            <path d="M14 4.5V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h5.5L14 4.5zm-3 0A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h-2z"/>
                                        </svg>
                                    </button>
                                </form>
                            @endif
                            <p>{{$comment->body}}</p>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
