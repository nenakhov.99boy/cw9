@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Редактирование статьи</h1>
        <div style="text-align: center">
            <form method="post" action="{{route('admin.articles.update', compact('article'))}}">
                @csrf
                @method('PUT')
                <div>
                    <p>Заголовок</p>
                    <textarea name="title" id="title" required>
                        {{$article->title}}
                    </textarea>
                    @error('title')
                    <div class="alert alert-danger">Заголовок обязателен и должен быть не менее 5 и не более 30 символов!</div>
                    @enderror
                </div>
                <div>
                    <p>Содержание</p>
                    <textarea name="body" id="body" required>{{$article->body}}</textarea>
                    @error('publish_time')
                    <div class="alert alert-danger">Содержание обязательно и должно быть не менее 100 и не более 500 символов!</div>
                    @enderror
                </div>
                <div>
                    <p>Выберите категорию</p>
                    <select name="category_id" id="category_id">
                        @foreach($categories as $category)
                            <option name="category_id" value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <p>Введите дату публикации</p>
                    <input type="datetime-local" id="publish_time" name="publish_time">
                </div>
                <input type="hidden" name="user_id" value="{{$article->user->id}}">
                <button type="submit">Обновить</button>
            </form>
        </div>
    </div>
@endsection
