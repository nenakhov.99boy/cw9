@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Редактирование статей</h1>
        <a class="btn btn-warning" href="{{route('admin.comments.index')}}">Раздел комментариев</a>
        <table>
            <thead>
                <tr>
                    <td>
                        Название статьи
                    </td>
                    <td>
                        Действия
                    </td>
                    <td>
                        Время публикации
                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($articles as $article)
                    <tr>
                    <td>
                        {{$article->title}}
                    </td>
                    <td>
                        <a class="btn btn-success" href="{{route('admin.articles.edit', compact('article'))}}">
                            Редактировать
                        </a>
                        <form method="post" action="{{route('admin.articles.destroy', compact('article'))}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Удалить</button>
                        </form>
                    </td>
                    <td>
                        @if($article->publish_time == null)

                        @else
                            {{date('Y/m/d H:i:s', $article->publish_time)}}
                        @endif
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="justify-content-md-center p-5">
            <div class="col-md-auto">
                {{ $articles->links()}}
            </div>
        </div>
    </div>
@endsection
