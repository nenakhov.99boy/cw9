@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Редактирование</h1>
        <form method="post" action="{{route('admin.comments.update', compact('comment'))}}">
            @csrf
            @method('PUT')
            <textarea name="body" id="body" placeholder="Напишите ваше мнение...">{{$comment->body}}</textarea><br>
            @error('body')
            <div class="alert alert-danger">Поле для заполнения комментария обязательно и должно содержать от 10 до 200 символов</div>
            @enderror
            <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
            <input type="hidden" name="article_id" value="{{$comment->article->id}}">
            <div>
                <p>Статус одобрения</p>
                <select name="is_approved_status" id="is_approved_status">
                    <option value="0">Ждёт одобрения</option>
                    <option value="1">Одобрен</option>
                </select>
            </div>
            <button>Отправить</button>
        </form>
    </div>
@endsection
