@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Раздел комменариев</h1>
        <a class="btn btn-warning" href="{{route('admin.home')}}">Раздел статей</a>
        <table>
            <thead>
            <tr>
                <td>
                    Автор комментария
                </td>
                <td>
                    Действия
                </td>
                <td>
                    Статус одобрения
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach($comments as $comment)
                <tr>
                    <td>
                        {{$comment->user->name}}
                    </td>
                    <td>
                        <a class="btn btn-success" href="{{route('admin.comments.edit', compact('comment'))}}">
                            Редактировать
                        </a>
                        <form method="post" action="{{route('admin.comments.destroy', compact('comment'))}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Удалить</button>
                        </form>
                    </td>
                    <td>
                        @if($comment->is_approved_status == null)
                            Ждёт одобрения
                        @else
                            Ододбрен
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="justify-content-md-center p-5">
            <div class="col-md-auto">
                {{ $comments->links()}}
            </div>
        </div>
    </div>
@endsection
