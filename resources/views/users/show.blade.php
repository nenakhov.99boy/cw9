@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Страница пользователя</h1>
        <div style="border: 1px solid black">
            <div style="margin-left: 50px">
                <h1 style="float: left; margin-right: 100px">
                    Имя: {{$user->name}}
                </h1>
                <h2 style="float: left; margin-right: 100px">Почта: {{$user->email}}</h2>
                <h2>Общий рейтинг: {{$rate}}</h2>
            </div>
        </div>
        @if($user->admin_status == 1)
        @endif
        <div style="margin-top: 100px">
            @if(\Illuminate\Support\Facades\Auth::id() == $user->id)
                <a class="btn btn-success" href="{{route('articles.create')}}">Создать новую статью</a>
            @endif
        </div>
    </div>
@endsection
