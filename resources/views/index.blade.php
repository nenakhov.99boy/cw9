@extends('layouts.app')
@section('content')
    <div style="border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black" class="container">
        <h1 style="text-align: center">Свежие новости!</h1>
        <div>
            <form method="post" action="{{route('filter')}}">
                @csrf
                Фильтры новостей по категориям
                <select name="category_id">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->title}}</option>
                    @endforeach
                </select>
                <button type="submit">фильтрация</button>
            </form>
        </div>
       @foreach($articles as $article)
           <div style="border: 1px solid black; margin-bottom: 20px">
               <a href="{{route('articles.show', compact('article'))}}" style="margin-left: 10px; font-size: 20px; color: black; text-decoration: none">{{$article->title}}</a>
               <p style="margin-left: 20px">{{$article->user->name}}</p>
           </div>
        @endforeach
    </div>
    <div class="justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $articles->links()}}
        </div>
    </div>
@endsection
