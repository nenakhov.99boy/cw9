<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/', [\App\Http\Controllers\Admin\AdminArticleController::class, 'index'])->name('home');
    Route::resource('articles', \App\Http\Controllers\Admin\AdminArticleController::class)->except('index', 'create', 'store', 'show');
    Route::resource('comments', \App\Http\Controllers\Admin\AdminCommentController::class)->except('create', 'store', 'show');
});

Route::get('/', [\App\Http\Controllers\ArticleController::class, 'index']);
Route::get('/users/{user}', [\App\Http\Controllers\UserController::class, 'show'])->name('profile');
Route::resource('articles', \App\Http\Controllers\ArticleController::class)->except('index');
Route::post('categoryFilter', [\App\Http\Controllers\ArticleController::class, 'filter'])->name('filter');
Route::patch('articleRate', [\App\Http\Controllers\ArticleController::class, 'rate'])->name('rate');
Route::resource('comments', \App\Http\Controllers\CommentsController::class)->except('index', 'create', 'show');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
