<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'bail' ,'min:5', 'max:100'],
            'body' => ['required', 'bail', 'min:100', 'max:500'],
            'category_id' => ['required'],
            'user_id' => ['required'],
            'publish_time' => ['date']
        ];
    }
}
