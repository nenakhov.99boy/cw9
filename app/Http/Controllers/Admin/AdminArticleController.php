<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleStoreRequest;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminArticleController extends Controller
{
    public function index()
    {
        \Illuminate\Support\Facades\Gate::authorize('is_admin');
        $articles = Article::paginate(10);
        return view('admin.articles.index', compact('articles'));
    }

    public function edit(Article $article)
    {
        \Illuminate\Support\Facades\Gate::authorize('is_admin');
        $categories = Category::all();
        return view('admin.articles.edit', compact('article', 'categories'));
    }

    public function update(ArticleStoreRequest $request, Article $article)
    {
        $user = Auth::user();
        $validated = $request->validated();
        $validated['publish_time'] = strtotime($validated['publish_time']);
        $article->update($validated);
        return redirect()->route('profile', compact('user'))->with('status', 'Статья обновлена');
    }



    public function destroy(Article $article)
    {
        \Illuminate\Support\Facades\Gate::authorize('is_admin');
        $article->delete();
        return redirect()->back()->with('status','Статья удалена');
    }
}
