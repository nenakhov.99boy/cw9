<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentStoreRequest;
use App\Models\Comment;
use Illuminate\Http\Request;

class AdminCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        \Illuminate\Support\Facades\Gate::authorize('is_admin');
        $comments = Comment::paginate(10);
        return view('admin.comments.index', compact('comments'));
    }


    public function edit(Comment $comment)
    {
        return view('admin.comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CommentStoreRequest $request, Comment $comment)
    {
        $validated = $request->validated();
        $comment->update($validated);
        return redirect()->route('admin.comments.index')->with('status', 'Комментарий обновлён');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect()->back()->with('status', 'Комментарий удалён');
    }
}
