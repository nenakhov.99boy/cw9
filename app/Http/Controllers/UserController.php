<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show (User $user)
    {
        $articles = $user->articles;
        $articles_amount = $articles->count();
        $articles_quality = [];
        $articles_actuality = [];
        foreach ($articles as $article)
        {
            $articles_actuality[] = $article->actuality_scale;
            $articles_quality[] = $article->quality_scale;
        }
        $quality_summary = array_sum($articles_quality);
        $actuality_summary = array_sum($articles_actuality);
        $rate = floor(($quality_summary + $actuality_summary + $articles_amount) / 3);
        return view('users.show', compact('user', 'rate'));
    }
}
