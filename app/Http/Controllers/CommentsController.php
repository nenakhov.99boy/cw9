<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentStoreRequest;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     */
    public function store(CommentStoreRequest $request)
    {
        $validated = $request->validated();
        $comment = new Comment($validated);
        $comment->save();
        return redirect()->back()->with('status', 'Комментарий будет опубликован после одобрения администрации');
    }

    public function edit(Comment $comment)
    {
        return view('comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CommentStoreRequest $request, Comment $comment)
    {
        $validated = $request->validated();
        $comment->update($validated);
        $article = $comment->article->id;
        return redirect()->route('articles.show', compact('article'))->with('status', 'Комментарий обновлён');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        $article = $comment->article->id;
        return redirect()->route('articles.show', compact('article'))->with('status', 'Комментарий удалён');
    }
}
