<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\RateStoreRequest;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ArticleController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $time = time();
        $articles = Article::whereRaw("publish_time < $time")->paginate(10);
        return view('index', compact('articles', 'time', 'categories'));
    }

    public function create()
    {
        $categories = Category::all();
        $user = Auth::id();
        return view('articles.create', compact('user', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ArticleStoreRequest $request)
    {
        $validated = $request->validated();
        $article = new Article($validated);
        $article->save();
        return redirect('/')->with('status', 'Статья отправлена к администрации!');
    }

    public function show(Article $article)
    {
        $timePub = date('Y/m/d H:i:s', $article->publish_time);
        $user = Auth::user();
        $author = $article->user();
        return view('articles.show', compact('article', 'user', 'timePub', 'author'));
    }

    public function edit(Article $article)
    {
        $user = Auth::id();
        $categories = Category::all();
        return view('articles.edit', compact('article', 'user', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ArticleStoreRequest $request, Article $article)
    {
        $user = Auth::user();
        $validated = $request->validated();
        $article->update($validated);
        return redirect()->route('profile', compact('user'))->with('status', 'Статья обновлена');
    }

    public function destroy(Article $article)
    {
        $user = Auth::user();
        $article->delete();
        return redirect()->route('profile', compact('user'))->with('status', 'Статья удалена');
    }

    public function filter(Category $category)
    {
        $categories = Category::all();
        $time = time();
        $articles = Article::whereRaw("publish_time > $time", "category_id == $category->id");
        $articles = $articles::whereRaw()->paginate(10);
        return view('index', compact('articles', 'time', 'categories'));
    }

    public function rate(RateStoreRequest $request, Article $article)
    {
        $validated = $request->validated();
        $like_scale = $article->likes_scale + $validated['like'];
        $quality_scale = $article->quality_scale + $validated['quality'];
        $actuality_scale = $article->actuality_scale + $validated['actuality'];
        $article->likes_scale = $like_scale;
        $article->actuality_scale = $actuality_scale;
        $article->quality_scale = $quality_scale;
        $article->title = $validated['title'];
        $article->body = $validated['body'];
        $article->category_id = $validated['category_id'];
        $article->user_id = $validated['user_id'];
        $article->update();
        return redirect()->back()->with('status', 'Благодарим за оценку!');
    }
}
